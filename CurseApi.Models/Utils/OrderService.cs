using System.Collections.Generic;


namespace CurseApi.Models.Utils
{
    public static class OrderService
    {
        private static List<Order> orders = Faker.RandomOrderSet;

        public static Order GetById(int id)
        {
            return orders.Find(o => o.Id == id);
        }

        public static Order[] GetAll()
        {
            return orders.ToArray();
        }
    }
}