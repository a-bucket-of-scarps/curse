using System.Collections.Generic;


namespace CurseApi.Models.Utils
{
    public static class Faker
    {
        public static Product[] InitFakeProductsData()
        {
            System.Random rand = new System.Random();
            
            return new Product[] {
                new StandartProduct(rand.Next(1000, 15000), 10, "Монитор HP"),
                new SupplierProduct(rand.Next(100, 150), 100, 28, "Бумага A4"),
                new StandartProduct(rand.Next(28000, 50000), 1, "Ноутбук toshiba"),
                new SupplierProduct(10, 10, rand.Next(100, 11000), "Ручка bayer")
            };
        }

        public static Product[] InitFakeProductsDataWithoutSuppliers()
        {
            System.Random rand = new System.Random();
            
            return new Product[] {
                new StandartProduct(rand.Next(1000, 15000), 10, "Монитор HP"),
                new StandartProduct(rand.Next(28000, 50000), 1, "Ноутбук toshiba"),
            };
        }

        public static Order InitFakeOrderData()
        {
            return new Order(InitFakeProductsData());
        }

        public static Order InitFakeOrderDataWithoutSuppliers()
        {
            return new Order(InitFakeProductsDataWithoutSuppliers());
        }


        public static List<Order> RandomOrderSet = new List<Order>{
            InitFakeOrderData(),
            InitFakeOrderDataWithoutSuppliers(),
        };
    }     
}
