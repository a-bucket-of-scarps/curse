namespace CurseApi.Models
{
    public abstract class Product
    {
        public Product(double price, int quantity, string name)
        {
            Price = price;
            Quantity = quantity;
            Name = name;
            orderId = null;

            CalculatePrice();
        }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public string Name { get; set; }

        public int? orderId { get; set; }

        public virtual void CalculatePrice() {}

    }


    public class SupplierProduct : Product
    {

        public SupplierProduct(double price, double supplierPrice, int quantity, string name) : base(price, quantity, name)
        {
            Price = price;
            SupplierPrice = supplierPrice;
            Quantity = quantity;
            Name = name;
            orderId = null;
            CalculatePrice();
        }
        public double SupplierPrice { get; set; }

        public override void CalculatePrice()
        {
            Price = SupplierPrice > Price ? Price : SupplierPrice;
        }
    }

    public class StandartProduct : Product 
    {
        public StandartProduct(double price, int quantity, string name) : base(price, quantity, name) {}
    }
}
