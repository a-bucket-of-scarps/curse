namespace CurseApi.Models
{
    public class Order
    {
        private static int counter = 0;
        public int Id;
        public Product[] products { get; set; }

        public bool IsProvidedBySuppliers
        {
            get
            {
                if (products == null) return false;

                foreach (Product prod in products)
                {
                    if (prod.GetType().Name == "SupplierProduct") return true;
                }
                return false;
            }
        }

        public Order(Product[] products) 
        {
            this.products = products;
            Id = ++counter;

            for (int i = 0; i < products.Length; i++) {
                this.products[i].orderId = Id;
            }
        }
    }
}
