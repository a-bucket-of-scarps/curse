using CurseApi.Models;
using CurseApi.Models.Utils;
using Microsoft.AspNetCore.Mvc;


namespace CurseApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        [HttpGet]
        public ActionResult<Order[]> Get()
        {

            string all = HttpContext.Request.Query["all"];
            string id = HttpContext.Request.Query["id"];

            if (all == null && id == null) return NotFound();

            if (System.Convert.ToBoolean(all) == true) return OrderService.GetAll();

            Order order = OrderService.GetById(System.Convert.ToInt32(id));
            if (order != null) return new Order[] { order };

            return NotFound();
        }
    }
}
